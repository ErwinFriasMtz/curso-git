# git branch
Una rama en git es simplemente un apuntador móvil apuntando a uno de los commits.

Puedo crear todas las ramas que quiera y/o necesite.

Las ramas nuevas que se crean apuntan al commit actual.

`git branch` : Muestra todas las ramas y en qué rama nos encontramos.

`git branch -v` : Muestra todas la ramas con su último commit y en qué rama nos encontramos.

## git merge branch_name
Con este comando podemos unir una rama a la rama principal del proyecto, para poder realizar este comando, debemos estar colocados en la rama principal.
## git branch --no-merged
Nos muestra cuáles ramas no han sido fusionadas a la rama actual.

## git branch --merged
Nos muestra que ramas que han sido fusionadas a la rama actual.
