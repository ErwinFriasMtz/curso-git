# git log
Muestra todo el historial de commits del proyecto

`git log --oneline` : Nos muestra el historial abreviado.

`git log --graph` : Añade un pequeño grafico ASCII mostrando el hostorial de ramificaciones y uniones.

`git log --pretty=format:"%h - %an, %ar : %s"` : Muestra la salida del historial con el formato que le indicamos.

## Limitar la salida del hostorial
`git log -n` : Cambiamos la n por cualquier número enteri, por ejemplo:  `git log -2` nos mostrará los 2 commits más recientes.

`git log --after="2017-24-12 00:00:00"` : Muestra los commits realizados después de la fecha especificada.

`git log --before="2017-23-12 00:00:00"` : Muestra los commits realizados antes de la fecha especificada.

Las banderas del comando `git log` se pueden usar juntas según nos convenga, por ejemplo:

`git log --after="2017-23-12 22:00:00" --before="2017-24-12 09:00:00"`

`git log --decorate --oneline --all --graph`
Este comando nos muestra el historial en una sola línea por commit.
