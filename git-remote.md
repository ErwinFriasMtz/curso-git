# git remote add origin https://github.com/ErwinFriasMtz/curso-git.git
Con este comando vinculamos nuestro repositorio local con GitHub.

## Pasos para aportar a otro repositorio
1. Hacer un fork en GitHub.
2. Clonar el repositorio desde mi cuenta de GitHub.
3. Crear una rama local.
4. Realizar mis cambios en mi nueva rama local.
5. Confirmar los cambios realizados en local.
6. Hacer push de mis cambios (enviar los commits localesa GitHub) `git push origin nombre_rama`.
7. Crear un pull request con la nueva rama de mi repositorio en GitHub.
8. Esperar a que el admin del repositorio original acepte mi pull request.

## Varios repositorios remotos
Podemos configurar un mísmo proyecto para sincronizar cambios con varios repositorios remotos.
