#git tag
Lista las etiquetas existentes.

## git tag nombre_etiqueta
Lista las etiquetas en orden alfabético.

### git tag -a nombre_etiqueta -m "mensaje de la etiqueta"
Etiqueta anotada. Seguarda en la base de datos de Git como objeto entero. Tienen un checksum; contienen el nombre del etiquetados, correo electrónico y fecha; y tienen un mensaje asociado.

```
git tag -l "v1.*"
```
Lista las etiquetas que coincidan con el patrón solicitado.
